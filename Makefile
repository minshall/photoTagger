ORGFILE = photoTagger.org

# R package
# so:
#
#1) tangle files into package subdirectory
#
# 2) roxygenize in that subdirectory
#
# 3) R cmd 

PKG = photoTagger

# where R package parts lie
PACKAGEDIR = ./r-package
# where gitlab pages lives
DISTDIR = ./public
URLDISTDIR = https://minshall.gitlab.io/photoTagger

PKGDIR = ${PACKAGEDIR}/${PKG}

PACKAGEJSON = ./package.json
FLOWTYPEDDIR = flow-typed/npm

# we use the npm version as our version, just because it seems pretty
# easy to access (with jq(1))
PKGVERS = $(shell jq .version ${PACKAGEJSON})

RELPKGTARGZ = ${PKG}_${PKGVERS}.tar.gz
PKGTARGZ = ${PACKAGEDIR}/${RELPKGTARGZ}
URLPKGTARGZ = ${URLDISTFILESDIR}/${RELPKGTARGZ}
## my routr:: targz file
RELMYROUTRTARGZ = routr_0.3.0.9999.2.tar.gz
BRANCHMYROUTR = release
PARENTDIRROUTR = ${PACKAGEDIR}
DIRROUTR = ${PARENTDIRROUTR}/routr
MYROUTRTARGZ = ${PARENTDIRROUTR}/${RELMYROUTRTARGZ}
URLMYROUTRTARGZ = ${URLDISTFILESDIR}/${RELMYROUTRTARGZ}

# subdirectories of package directory
PKGRDIR = ${PKGDIR}/R
PKGMANDIR = ${PKGDIR}/man
PKGINSTDIR = ${PKGDIR}/inst
PKGINSTDOCDIR = ${PKGINSTDIR}/doc
PKGPRIVATEDIR = ${PKGINSTDIR}/private
PKGVIGNETTESDIR = ${PKGDIR}/vignettes
PKGINSTDIR = ${PKGDIR}/inst
PKGBINDIR = ${PKGINSTDIR}/bin
PKGEXAMPLESDIR = ${PKGINSTDIR}/examples
PKGEXAMPLESSITEDIR = ${PKGEXAMPLESDIR}/site
PKGEXAMPLESSRCDIR = ${PKGEXAMPLESDIR}/src
PKGEXAMPLESMKDIR = ${PKGEXAMPLESDIR}/makefile
PKGSRCDIR = ${PKGINSTDIR}/src
PKGSRCDIRJS = ${PKGSRCDIR}/js
PKGSRCDIRCSS = ${PKGSRCDIR}/css
PKGSRCDIRS = ${PKGSRCDIRJS} ${PKGSRCDIRCSS}
PKGDISTDIR = ${PKGINSTDIR}/dist
PKGDISTDIRJS = ${PKGDISTDIR}/js
PKGDISTDIRCSS = ${PKGDISTDIR}/css
PKGDISTDIRS = ${PKGDISTDIRJS} ${PKGDISTDIRCSS}

ROUTRDIR = ${PACKAGEDIR}/routr
ROUTRDIRS = ${ROUTRDIR}

RELFILESDIR = files
DISTFILESDIR = ${DISTDIR}/${RELFILESDIR}
URLDISTFILESDIR = ${URLDISTDIR}/${RELFILESDIR}
DISTINDEXORG = ${DISTDIR}/index.org
DISTINDEXHTML = ${DISTDIR}/index.html
DISTVIEWERORG = ${DISTDIR}/viewer.org
DISTDIRS = ${DISTDIR} ${DISTFILESDIR} ${DISTDEMODIR}
DISTORGFILES = ${DISTINDEXORG} ${DISTVIEWERORG}
DISTHTMLFILES = $(subst .org,.html,${DISTORGFILES})
DISTTANGLED = ${DISTORGFILES}
DISTMYROUTRTARGZ = ${DISTFILESDIR}/${RELMYROUTRTARGZ}
DISTPKGTARGZ = ${DISTFILESDIR}/${RELPKGTARGZ}
RELDOCKERFILE = dockerfile.photoTagger
DISTDOCKERFILE = ${DISTFILESDIR}/${RELDOCKERFILE}
URLDOCKERFILE = ${URLDISTFILESDIR}/${RELDOCKERFILE}

DEMODIR = ${PACKAGEDIR}/demo
RELDEMODEMODIR = demo-directory
DEMODEMODIR = ${DEMODIR}/${RELDEMODEMODIR}
DEMOACTIONS = ${DEMODEMODIR}/someactions.json
DEMOIMAGES = ${DEMODEMODIR}/images
DEMOSCALED = ${DEMODEMODIR}/scaled
DEMOTHUMBS = ${DEMODEMODIR}/thumbnails
RELDEMOTARGZ = demo-directory.tar.gz
DISTDEMOTARGZ = ${DISTFILESDIR}/${RELDEMOTARGZ}
URLDEMOTARGZ = ${URLDISTFILESDIR}/${RELDEMOTARGZ}
DISTDEMODIR = ${DISTDIR}/${RELDEMODEMODIR}
DISTDEMOPARENTDIR = ${abspath ${DISTDIR}/${RELDEMODEMODIR}/..}
# demo images, random, from wikipedia
DEMOWIKIIMAGEURL = https://commons.wikimedia.org/wiki
DEMOIMAGE1PATH = Dan_ONeill_1982.jpg
DEMOIMAGE1URL = https://upload.wikimedia.org/wikipedia/commons/b/bd
DEMOSCALED1PATH = ../scaled/${DEMOIMAGE1PATH}
DEMOTHUMB1PATH = ../thumbnails/${DEMOIMAGE1PATH}
DEMOIMAGE1ATTR = photo by Alan Light, CC BY 2.0 <https://creativecommons.org/licenses/by/2.0>, via Wikimedia Commons
DEMOIMAGE2PATH = Alexander_horned_sphere.png
DEMOIMAGE2URL = https://upload.wikimedia.org/wikipedia/commons/0/0a/
DEMOSCALED2PATH = ../scaled/${DEMOIMAGE2PATH}
DEMOTHUMB2PATH = ../thumbnails/${DEMOIMAGE2PATH}
DEMOIMAGE2ATTR = Dist domain, via Wikimedia Commons
DEMOIMAGE3PATH = "ACC_The_Accent_Wavy_Gravy_passing_out_free_ice_cream_(3377996458).jpg"
DEMOIMAGE3URL = https://upload.wikimedia.org/wikipedia/commons/5/56/
DEMOSCALED3PATH = ../scaled/${DEMOIMAGE3PATH}
DEMOTHUMB3PATH = ../thumbnails/${DEMOIMAGE3PATH}
DEMOIMAGE3ATTR = The Accent from USA, CC BY-SA 2.0 <https://creativecommons.org/licenses/by-sa/2.0>, via Wikimedia Commons

DEMODIRS = ${DEMODIR} ${DEMODEMODIR} ${DEMOIMAGES} ${DEMOSCALED} ${DEMOTHUMBS}

DEMOTANGLED = ${DEMODEMODIR}/someactions.json
NODEMODULES = ./node_modules

# library ("include") file for js "flow" lint tool
FLOWTYPEDNAMES =  copy-to-clipboard_v3.x.x.js \
					jquery_v3.x.x.js \
					@ungap/promise-all-settled_v1.1.x.js
FLOWTYPEDFILES =  $(addprefix ${FLOWTYPEDDIR}/,${FLOWTYPEDNAMES})

PKGDIRS = ${PKGDIR} ${PKGRDIR} \
				${PKGINSTDIR} ${PKGPRIVATEDIR} ${PKGINSTDOCDIR} \
				${PKGBINDIR} \
				${PKGEXAMPLESDIR} \
				${PKGEXAMPLESSITEDIR} ${PKGEXAMPLESSRCDIR} ${PKGEXAMPLESMKDIR} \
				${PKGVIGNETTESDIR} ${PKGSRCDIRS} ${PKGDISTDIRS}

# web bits (see comment at top of file for provenance)
# Install `babel-cli` in a project to get the transpiler.

# npm and npx: favor from node_modules/.bin (locally installed) over
# globally, as locally is more likely to be(come) up to date
NPM := $(shell if [ -x ${NODEMODULES}/.bin/npm ]; then \
		echo ${NODEMODULES}/.bin/npm; \
	elif (which npm &> /dev/null); then \
		echo npm; \
	fi)
NPX := $(shell if [ -x ${NODEMODULES}/.bin/npx ]; then \
		echo ${NODEMODULES}/.bin/npx; \
	elif (which npm &> /dev/null); then \
		echo npx; \
	fi)

$(condition $(if $(and ${NPM},${NPX}),,$(error "javascript package manager npm (and npx) not found")))

BABEL := ${NPX} babel
SNOWPACK := ${NPX} snowpack
FLOW := ${NPX} flow

SNOWPACKCONFIG = ./snowpack.config.json

# files to be processed by =flow=
FLOW_FILES = ${PKGSRCDIRJS}/photoTagger.js
FLOWN_FILES = $(addprefix ${PKGDISTDIRJS}/,$(notdir ${FLOW_FILES}))

# the idea here is, SRC dir is where files go to be targets of flow;
# if they pass that, then the go to DIST dir
${PKGDISTDIRJS}/% : ${PKGSRCDIRJS}/%
	${FLOW}
	cp -p $< $@

PKGRFILES = ${PKGRDIR}/photoTagger.R

CLIRFILES = ${PKGBINDIR}/pt-add ${PKGBINDIR}/pt-tag \
			${PKGBINDIR}/pt-view ${PKGBINDIR}/pt-viewer-build

PKGPACKAGEJSON = ${PKGDIR}/package.json

# we use the roxygen2 to create documentation
PKGNAMESPACE = ${PKGDIR}/NAMESPACE
PKGMAN =
ROXYGENFILES = ${PKGNAMESPACE} ${PKGMAN}

PKGTANGLED = ${PKGDIR}/DESCRIPTION.template ${PKGINSTDIR}/CITATION \
					${PKGDIR}/LICENSE ${PKGRFILES} ${CLIRFILES}

PKGFILES = ${PKGTANGLED} ${ROXYGENFILES} ${FLOWN_FILES} ${PKGEXAMPLESMKDIR}/Makefile.mk

MAKE_FAKES_DIR = ./make-fakes
MAKE_FAKES_TANGLE = ${MAKE_FAKES_DIR}/tangle
MAKE_FAKES_SNOWPACKED = ${MAKE_FAKES_DIR}/snowpacked
MAKE_FAKES_PKGLINTR = ${MAKE_FAKES_DIR}/pkglintr
MAKE_FAKES_CLILINTR = ${MAKE_FAKES_DIR}/clilintr
MAKE_FAKES_TARGETS = ${MAKE_FAKES_TANGLE} ${MAKE_FAKES_SNOWPACKED} \
						${MAKE_FAKES_PKGLINTR} ${MAKE_FAKES_CLILINTR}

# we tangle files here, only copy as changed
TANGLEWOOD = ${MAKE_FAKES_DIR}/tanglewood

SKELDIRS = ${PKGDIRS} ${TANGLEWOOD} ${DISTDIRS} ${DEMODIRS} ${ROUTRDIRS}

TANGLED = ${PKGTANGLED} ${DISTTANGLED} ${DEMOTANGLED} ${FLOW_FILES}

# targets

# '|': 'order-only' prerequisite
.PHONY : all
all: devenv ${PKGTARGZ} check
	@echo "package NOT installed, by the way"

install: devenv ${PKGTARGZ} check
	cd ${PACKAGEDIR} && Rscript -e "install.packages(\"${RELPKGTARGZ}\")"

# just echo the version
.PHONY : version
version:
	@echo ${PKGVERS}

.PHONY : tgzname
tgzname:
	@echo ${PKGTARGZ}

.PHONY : files
files: ${PKGPACKAGEJSON}

# set up things in a brand new repository whatever
devenv: skeldirs ${NODEMODULES} flowtyped

${SKELDIRS}:: ${PACKAGEDIR}
	@for dir in ${SKELDIRS}; do \
		if [ ! -e $${dir} ]; then mkdir -p $${dir}; fi \
	done

.PHONY : skeldirs
skeldirs: ${SKELDIRS}

${PKGDIR}/DESCRIPTION: ${PKGDIR}/DESCRIPTION.template

${PKGEXAMPLESMKDIR}/Makefile.mk: Makefile.mk
	cp -p $< $@

${PACKAGEDIR}:
	if [ ! -e ${PACKAGEDIR} ]; then \
		mkdir -p ${PACKAGEDIR}; \
		for i in ${MAKE_FAKES_TARGETS}; do \
			rm -f ${i}; \
		done; \
	fi

# rsync --recursive for ./: http://lucasb.eyer.be/snips/rsync-skipping-directory.html

RSYNC_FLAGS = --checksum --recursive --max-delete=-1
# just to be paranoid (should be set above; and, this is the wrong setting)
TANGLEWOOD ?= .
${MAKE_FAKES_TANGLE}: ${ORGFILE} ${PACKAGEJSON}
	./dotangle.el ${ORGFILE}
	for tmpl in `find ${TANGLEWOOD} -name "*.template"`; do \
		nontmpl=`echo $$tmpl | sed 's/[.]template$$//'`; \
		cat $${tmpl} | \
		sed "s|__RELPKGTARGZ__|${RELPKGTARGZ}|" | \
		sed "s|__URLPKGTARGZ__|${URLPKGTARGZ}|" | \
		sed "s|__RELMYROUTRTARGZ__|${RELMYROUTRTARGZ}|" | \
		sed "s|__URLMYROUTRTARGZ__|${URLMYROUTRTARGZ}|" | \
		sed "s|__RELDEMOTARGZ__|${RELDEMOTARGZ}|" | \
		sed "s|__URLDEMOTARGZ__|${URLDEMOTARGZ}|" | \
		sed "s|__RELDEMODEMODIR__|${RELDEMODEMODIR}|" | \
		sed "s|__PKGVERS__|${PKGVERS}|" > $${nontmpl}; \
		rm $${tmpl}; \
	done
	rsync ${RSYNC_FLAGS} ${TANGLEWOOD}/ .
	@touch $@ # try to reduce the number of times we do this...

.PHONY: tangle
tangle: | ${MAKE_FAKES_TANGLE}

# the semi-colon is important, at least with current make: without it,
# things that depend on ${TANGLED} may not be built
# https://savannah.gnu.org/bugs/?59490 is a bug report i made.
${TANGLED}: tangle;

# pass a copy on down to the package
${PKGPACKAGEJSON}: ${PACKAGEJSON}
	rm -f $@
	cp -p $< $@

${FLOWTYPEDDIR}/%:
	${NPX} flow-typed install `echo $@ | sed 's/_/@/;s/\.js//;sX^${FLOWTYPEDDIR}/XX'`

flowtyped: tangle ${NODEMODULES} ${FLOWTYPEDFILES}

${NODEMODULES}:
	${NPM} install --also=dev

${SNOWPACKCONFIG}: tangle;

${PKGTARGZ}: ${PKGFILES} | ${PACKAGEDIR} snowpacked
	cd ${PACKAGEDIR} && R CMD build ${PKG}

# 'check' checks the *tar.gz* file:
# https://r.789695.n4.nabble.com/Mysterious-NOTE-when-checking-package-tp4757346p4757348.html
check: ${PKGTARGZ} pkglintr clilintr
	cd ${PACKAGEDIR} && R CMD check ${RELPKGTARGZ}

# NOTE for "maintainer" can be ignored: https://stackoverflow.com/a/23831508
ascran: ${PKGTARGZ}
	cd ${PACKAGEDIR} && R CMD check --as-cran ${RELPKGTARGZ}

${ROXYGENFILES}: ${ORGFILE} tangle | ${SKELDIRS}
	cd ${PKGDIR} && Rscript -e 'require(roxygen2, quietly=TRUE); roxygenize()'

.PHONY: roxygenize
roxygenize: ${ROXYGENFILES}

# lint the R code; result in an error if lintr:: returns any warnings, etc.
${MAKE_FAKES_PKGLINTR}: ${PKGRFILES}
	@echo linting package R code
	@Rscript -e "require(lintr, quietly = TRUE); for (f in commandArgs(TRUE)) { y <- lint(f); if (length(y)) { print(y); quit(status=length(y))}}" ${PKGRFILES}
	@touch $@

pkglintr: ${MAKE_FAKES_PKGLINTR}

# this is slightly tricky since it can't run unless the package has
# been installed.  so, we source our .R file (we know there is just
# one), then lint
${MAKE_FAKES_CLILINTR}: ${CLIRFILES}
	echo linting command line R code;
	@Rscript -e "source('${PKGRFILES}'); require(lintr, quiet=TRUE); for (f in commandArgs(TRUE)) { y <- lint(f); if (length(y)) { print(y); quit(status=length(y))}}" ${CLIRFILES}
	touch $@;

clilintr: ${MAKE_FAKES_CLILINTR}

.PHONY: npmupdate
npmupdate: ${PACKAGEJSON} ${NODEMODULES}
	${NPM} update --also=dev

.PHONY: flowtypeupdate
# seems broken XXX
flowtypeupdate: ${PACKAGEJSON} ${NODEMODULES}
	@echo XXX NOT DOING ${NPX} flow-typed update

# snowpack: https://www.snowpack.dev/ actually build, then "install"
# (to get "install" files -- e.g. bootstrap .css file -- in the right
# place).  there's a bug (?) in snowpack
# https://github.com/snowpackjs/snowpack/issues/1286 that keeps it
# from mounting things in node_modules.  so, we do that on our own.
#
# one of the two rsync's is unnecessary, though i'm in transition at
# the moment, so i'm not sure which one (probably the first?)
${MAKE_FAKES_SNOWPACKED}: devenv tangle ${SNOWPACKCONFIG} ${PACKAGEJSON} ${FLOWN_FILES} | ${PKGLIBDIR} files npmupdate flowtypeupdate
	${SNOWPACK} build
	rsync --info=NONE -avz ${NODEMODULES}/lightgallery/dist/{fonts,img} r-package/photoTagger/inst/examples/site/
	rsync --info=NONE -avz ${NODEMODULES}/lightgallery/dist/{fonts,img} r-package/photoTagger/inst/examples/site/_snowpack/pkg/lightgallery/dist
	@touch ${MAKE_FAKES_SNOWPACKED}

snowpacked: ${MAKE_FAKES_SNOWPACKED}

# this starts up something that watches for changes and reloads browwer
.PHONY: snowpackdev
snowpackdev: devenv tangle ${SNOWPACKCONFIG} ${PACKAGEJSON} | files npmupdate flowtypeupdate
	${SNOWPACK} dev

# various git utilities

# the current branch.  ':=' so it is evaluated only on startup
GITCURRBRANCH := $(shell git branch --show-current)

releasework: gitgoodtogo
	git checkout release
	git merge -m "merge from master" master
	git push

releaseversion:
	@echo released version `cat ${PACKAGEJSON} | jq -r '.version'`

backup:							# make a copy of ${PACKAGEJSON}
	cp -p ${PACKAGEJSON} ${PACKAGEJSON}.backup

# release one of: major, minor, patch
releasemajor: gitgoodtogo bumpmajor gitcommitrelease releasework gitcoprev releaseversion
releaseminor: gitgoodtogo bumpminor gitcommitrelease releasework gitcoprev releaseversion
releasepatch: gitgoodtogo bumppatch gitcommitrelease releasework gitcoprev releaseversion

# constant bits of the jq expression.  jq magic:
# https://stedolan.github.io/jq/tutorial/
JQTONUM='.|.version=([[(.version/".")|.[]|tonumber]|'
JQTOSTR='|.[]|tostring]|.[0]+"."+.[1]+"."+.[2])'
# modify version number in various ways
bumpmajor: backup
	jq ${JQTONUM}'[.[0]+1, 0, 0]'${JQTOSTR} < ${PACKAGEJSON}.backup > ${PACKAGEJSON}
bumpminor: backup
	jq ${JQTONUM}'[.[0], .[1]+1, 0]'${JQTOSTR} < ${PACKAGEJSON}.backup > ${PACKAGEJSON}
bumppatch: backup
	jq ${JQTONUM}'[.[0], .[1], .[2]+1]'${JQTOSTR} < ${PACKAGEJSON}.backup > ${PACKAGEJSON}


gitgoodtogo: gitcleanp gitmasterp

gitcleanp:
	@git status --short | awk '$$1 !~ /[?][?]/ { x++ } END {exit x}' || \
			(echo "ERROR: git working tree not clean" > /dev/stderr; exit 1)

# https://gist.github.com/justintv/168835#gistcomment-3012111
gitmasterp:
	@[[ `git symbolic-ref --short HEAD` = "master" ]] || \
			(echo "ERROR: git not on branch master" > /dev/stderr; exit 1)

# checkout the branch we were on when make launched
gitcoprev:
	git checkout ${GITCURRBRANCH}

gitcommitrelease:
	git commit -m "roll a release" ${PACKAGEJSON}

# we want org-html-toplevel-hlevel to be non-standard.  setting it as
# a local file variable doesn't work with --batch.  so...
# https://stackoverflow.com/a/25194923/1527747
# to get a nice, bootstrap'py feel:
# https://github.com/marsmining/ox-twbs

${DISTDIR}/%.html: ${DISTDIR}/%.org
	@echo creating $@
	@emacs \
		-L `find ~/.emacs.d/ -name "org-[0-9]*" -type d` -l org \
		-L `find ~/.emacs.d/ -name "htmlize*" -type d` -l htmlize \
		--batch $< --eval " \
			(progn \
				(org-babel-do-load-languages \
					'org-babel-load-languages \
					'((shell . t)) \
				) \
				(let ((org-confirm-babel-evaluate nil)) \
					(org-html-export-to-html)) \
				(if noninteractive \
					(kill-emacs))) \
		"

disthtmlfiles: ${DISTHTMLFILES}

# a dockerfile, for our and others use...
${DISTDOCKERFILE}: .gitlab-ci.yml ./dockerfilebuild
	./dockerfilebuild < $< > $@

dockerfile: ${DISTDOCKERFILE}

# clean up after ourselves
clean:
	@rm -f ${MAKE_FAKES_TARGETS}
	@rm -f ${PKGFILES}
	@rm -rf ${TANGLEWOOD}
	@rm -rf ${DEMODIR}
	@rm -rf ${PKGDIR} ${PKGDIR}.Rcheck
	@rm -rf ${DISTDIR}

distclean: clean
	@rm -f ${PKGTARGZ}

## some bits just for gitlab pages, not normally executed

# built thumbnails in built directory
THUMBNAIL_GEOM=300x300
${BUILT_THUMBNAILS_DIR}/% : ${SRC_IMAGES_DIR}/%
	convert $< -resize ${THUMBNAIL_GEOM} $@

## demo tar file
distdemotargz: ${DISTDEMOTARGZ}
${DISTDEMOTARGZ}: ${DEMODIR} ${DEMODEMODIR} ${DEMOTANGLED}
	cd ${DEMOIMAGES} && if [ ! -e ${DEMOIMAGE1PATH} ]; then \
		wget -O ${DEMOIMAGE1PATH} ${DEMOIMAGE1URL}/${DEMOIMAGE1PATH}; \
		convert ${DEMOIMAGE1PATH} -resize 800x800 ${DEMOSCALED1PATH}; \
		convert ${DEMOIMAGE1PATH} -resize ${THUMBNAIL_GEOM} ${DEMOTHUMB1PATH}; \
	fi
	cd ${DEMOIMAGES} && if [ ! -e ${DEMOIMAGE2PATH} ]; then \
		wget -O ${DEMOIMAGE2PATH} ${DEMOIMAGE2URL}/${DEMOIMAGE2PATH}; \
		convert ${DEMOIMAGE2PATH} -resize 800x800 ${DEMOSCALED2PATH}; \
		convert ${DEMOIMAGE2PATH} -resize ${THUMBNAIL_GEOM} ${DEMOTHUMB2PATH}; \
	fi
	cd ${DEMOIMAGES} && if [ ! -e ${DEMOIMAGE3PATH} ]; then \
		wget -O ${DEMOIMAGE3PATH} ${DEMOIMAGE3URL}/${DEMOIMAGE3PATH}; \
		convert ${DEMOIMAGE3PATH} -resize 800x800 ${DEMOSCALED3PATH}; \
		convert ${DEMOIMAGE3PATH} -resize ${THUMBNAIL_GEOM} ${DEMOTHUMB3PATH}; \
	fi
	curdir=$${PWD} && cd ${DEMODIR} && tar zcf $${curdir}/${DISTFILESDIR}/${RELDEMOTARGZ} demo-directory

# just for fun, in common, initialize and add photos
# then, separately, create one bs, one lg
distdemo: ${DISTDEMOTARGZ}
	cat ${DISTDEMOTARGZ} | (cd ${DISTDEMOPARENTDIR}; tar zxf -)
	cd ${DISTDEMODIR} && \
	    export PTGGR=`Rscript -e "cat(system.file('.', package='photoTagger'))"` && \
		make -f $${PTGGR}/examples/makefile/Makefile.mk SRC_TAGS=./sometags.json init add
	cd ${DISTDEMODIR} && \
	    export PTGGR=`Rscript -e "cat(system.file('.', package='photoTagger'))"` && \
	    make -f $${PTGGR}/examples/makefile/Makefile.mk \
				SRC_TAGS=./sometags.json \
				GALLERY_KIND=bs \
				BUILD_DIR=bs \
				FILTERS_STYLE=listing \
				TITLE="photoTagger viewer; bs with FILTERS_STYLE=listing"  \
				viewerbuild
	cd ${DISTDEMODIR} && \
	    export PTGGR=`Rscript -e "cat(system.file('.', package='photoTagger'))"` && \
		make -f $${PTGGR}/examples/makefile/Makefile.mk \
				SRC_TAGS=./sometags.json \
				GALLERY_KIND=lg \
				BUILD_DIR=lg \
				FILTERS_STYLE=checkboxes \
				TITLE="photoTagger viewer; lg with FILTERS_STYLE=checkboxes"  \
				viewerbuild

rmdistdemo:
	if [[ "${DISTDEMODIR}" ]] && [[ -d "${DISTDEMODIR}" ]]; then \
		rm -rf "${DISTDEMODIR}"; \
	fi

# my version of routr::.  i should make the version (in DESCRIPTION)
# unique to me.  increment as file changes.
myroutrtargz : ${MYROUTRTARGZ}
${MYROUTRTARGZ}:
	if [ -e "${DIRROUTR}" ]; then rm -rf ${DIRROUTR}; fi
	cd ${PARENTDIRROUTR} && \
		git clone --branch ${BRANCHMYROUTR} https://gitlab.com/minshall/routr.git
	cd ${PARENTDIRROUTR} && R CMD build routr
${DISTMYROUTRTARGZ}: ${MYROUTRTARGZ}
	cp -p $< $@
distmyroutrtargz: ${DISTMYROUTRTARGZ}

# this package
${DISTPKGTARGZ}: ${PKGTARGZ}
	cp -p $< $@
distpkgtargz: ${DISTPKGTARGZ}

dist: distpkgtargz distdemotargz distmyroutrtargz disthtmlfiles distdemo dockerfile
