# To configure this Makefile.mk, specify the following variables:

# database, with git repository
SRC_DATABASE ?= ./database/database.json
# where images come from
SRC_IMAGES_DIR ?= ./images
# tags file
SRC_TAGS ?= ./tags.json


# this makefile has the following targets you might use:
#
# init: creates a new git repository, with an empty database
#
# add: after initializing, adds images from SRC_IMAGES_DIR to the
#   database
#
# tag: runs a local server and launches a browser for you to tag the
#   images in your database
#
# view: runs a local server to view the images
#
# viewerbuild: creates a *static* site for you that allows you to view
#   images and filter (by tags) which ones you see
#
# viewerclean: removes files (including the web site directory)
#   created by the viewerbuild process


# some variables you may want to change (before including this file):

# title of the built web page
TITLE ?= "photoTagger viewer"

# there are two kinds of galleries, one ("lg") using the jquery
# "lightgallery" package, and the other ("bs") using the Bootstrap 4
# "carousel" component.  "lg" is probably nicer for viewing, but
# currently -- see https://gitlab.com/minshall/photoTagger/-/issues/22
# -- "bs" is better for tagging.
GALLERY_KIND ?= "bs"

# there are two ways of showing the tags to (potentially) filter as
# they come and go.  the most intuitive, probably, is the default,
# "checkboxes".  but, if you have lots of tags, the "listing" style --
# where tags that have been applied to an image are shown in a list,
# and new ones added, or existing deleted, using their "tag"
# keystroke -- may be easier.
FILTERS_STYLE ?= "checkboxes"

# where everything is built
BUILD_DIR ?= ./built

# size of thumbnails
THUMBNAIL_GEOM ?= 300x300

# if you want to prefilter images (see the documentation), the
# argument for the --pre-filter option (for pt-tag, pt-view, and
# pt-viewer-build) should be specified
PREFILTER ?=

# similarly, if you want to have the client (web browser) randomize
# some selection of the images, the argument for the --randomize
# option (to pt-view and pt-viewer-build) should be specified
RANDOMIZE ?=

# if you want to specify the fraction of images that will come from
# the randomized group of images (either as a single fraction, or as
# `min:max`), the value for the argument to the --randomize-frac can
# be specified here
RANDOMIZEFRAC ?=

# in various commands, more verbose output
VERBOSE ?=

THIS_MAKEFILE := $(lastword $(MAKEFILE_LIST))

# the rest is general, though it reflects a particular structure.

# BUILT_ is the prefix for things we build.  they can be safely "blown
# away" by a clean: target

# REL_ are directory-local file names, $(notdir) in (g)make-parlance;
# basename(1) for the shell

# SRC_ are inputs to the process, things we do *NOT* want to delete
# during, e.g., a [make clean]


# to change some parameters for the tag: and view: targets, you can
# change variables on the make command line:

# you can set "VERBOSE=--verbose" to get debugging output

# you can set "BROWSER=--no-browse" to not launch a browser

# you can set "PERSIST = --persist" to have the server persist over
# client browser sessions

# HELP

# a little introspection
help:
	@echo -e "\tthe following make variables can be defined when invoking make"
	@echo -e "\tlike: make TITLE=\"my viewer site\" viewerBuild"
	@echo -e "\t(default in brackets)"
	@echo
	@cat ${THIS_MAKEFILE} | \
		awk ' \
			BEGIN{ \
				i=1 \
			} \
			/^#/ { \
				add[i] = $$0; \
				i++ \
			} \
			/^[A-Z_0-9]+ *[?]=/ { \
				split($$0, a, /[?]=/); \
				gsub("^ ", "", a[2]); \
				print sprintf("%s [%s]", a[1], a[2]); \
				for (j = 1; j < i; j++) { \
					sub("^# ", "", add[j]); \
					print "\t", add[j]; } i=1 } \
			/^[^#]/ { \
				i = 1} \
			/^$$/ { \
				i = 1 \
			}'




# things that are common between tagger and viewer (currently unused)
BUILT_COMMON_DIR = ${BUILD_DIR}/for_both
# where we put things on their way to the viewer site (so, output from
# make, input to buildViewer)
BUILT_FOR_VIEWER_DIR = ${BUILD_DIR}/for_viewer
# where we put things on their way to the tagger
BUILT_FOR_TAGGER_DIR = ${BUILD_DIR}/for_tagger
# where the photo viewer site is built
BUILT_VIEWER_SITE_DIR = ${BUILD_DIR}/viewerSite
# two subdirectories we need to populate
BUILT_VIEWER_SITE_IMAGES_DIR = ${BUILT_VIEWER_SITE_DIR}/images
BUILT_VIEWER_SITE_THUMBS_DIR = ${BUILT_VIEWER_SITE_DIR}/thumbnails
# database name
REL_DATABASE = $(notdir ${SRC_DATABASE})
# database (tagger's is from src...)
BUILT_VIEWER_DATABASE = ${BUILT_FOR_VIEWER_DIR}/${REL_DATABASE}

# given that it's a bit odd, make sure jq(1) is installed on the
# system, warn if it isn't
JQ := $(shell if (which jq &> /dev/null); then \
		echo jq; \
	fi)

$(if ${JQ},,$(error "required jq(1) command not found"))

# tagger's images (tagger's images are from src_...)
REL_TAGGER_IMAGES = $(subst ",, $(shell [ -f ${SRC_DATABASE} ] && ${JQ} '.[]|.path' ${SRC_DATABASE})) # "
SRC_TAGGER_IMAGES = $(addprefix ${SRC_IMAGES_DIR}/,${REL_TAGGER_IMAGES})
# where thumbnails come from
BUILT_TAGGER_THUMBS_DIR = ${BUILT_FOR_TAGGER_DIR}/thumbnails
BUILT_TAGGER_THUMBS = $(addprefix ${BUILT_TAGGER_THUMBS_DIR}/,${REL_TAGGER_IMAGES})

# viewer's thumbnails
BUILT_VIEWER_IMAGES_DIR = ${BUILT_FOR_VIEWER_DIR}/images
REL_VIEWER_IMAGES = $(subst ",, $(shell [ -f ${BUILT_VIEWER_DATABASE} ] && ${JQ} '.[]|.path' ${BUILT_VIEWER_DATABASE})) # "
BUILT_VIEWER_IMAGES = $(addprefix ${BUILT_VIEWER_IMAGES_DIR}/,${REL_VIEWER_IMAGES})
# viewer's thumbnails
BUILT_VIEWER_THUMBS_DIR = ${BUILT_FOR_VIEWER_DIR}/thumbnails
BUILT_VIEWER_THUMBS = $(addprefix ${BUILT_VIEWER_THUMBS_DIR}/,${REL_VIEWER_IMAGES})

# if PREFILTER was specified, pass it on to the commands
OPT_PREFILTER = $(if ${PREFILTER},--pre-filter "${PREFILTER}")
# ditto for RANDOMIZE, RANDOMIZEFRAC
OPT_RANDOMIZE = $(if ${RANDOMIZE},--randomize "${RANDOMIZE}")
OPT_RANDOMIZEFRAC = $(if ${RANDOMIZEFRAC},--randomize-frac "${RANDOMIZEFRAC}")

# built thumbnails in built directory
${BUILT_TAGGER_THUMBS_DIR}/% : ${SRC_IMAGES_DIR}/%
	convert "$<" -resize ${THUMBNAIL_GEOM} "$@"

${BUILT_VIEWER_THUMBS_DIR}/% : ${SRC_IMAGES_DIR}/%
	convert "$<" -resize ${THUMBNAIL_GEOM} "$@"

${BUILT_VIEWER_IMAGES_DIR}/% : ${SRC_IMAGES_DIR}/%
	cp -p "$<" "$@"

# TAGS.JSON

# VIEWER DATABASE:

# jq(1) is amazing.  a bit tricky.

# echo '[{"tags": ["exclude", "aaa", "bbb"]}, {"tags": ["xxx", "yyy"]}]' | jq '.[] | select(debug | any(.tags | .[] | debug; debug | select(debug | . == "exclude"))|not)'

JQ_DEBUG = debug
JQ_DEBUG = .
JQ_PROGRAM = 'reduce (.[] | select(${JQ_DEBUG} | any(.tags | .[] | ${JQ_DEBUG}; ${JQ_DEBUG} | select(${JQ_DEBUG} | . == "${EXCLUDE}"))|not)) as $$item ([]; .+[$$item])'
${BUILT_VIEWER_DATABASE} : ${SRC_DATABASE}
	cat $< | if [ "${EXCLUDE}" == "" ]; then cat; else ${JQ} ${JQ_PROGRAM}; fi > $@

${BUILT_FOR_VIEWER_DIR}:
	mkdir -p $@
.PHONY: viewer_database
viewer_database: ${BUILT_FOR_VIEWER_DIR} ${BUILT_VIEWER_DATABASE}


# generics: https://stackoverflow.com/a/57989396/1527747 (and, the
# make info pages/manual).  these are more complicated, but seem
# better than replicating the same code

# notice that in generics, *shell* variables need *four* dollar signs:
# after $(call), *two* are left, which is what one wants for $(eval)

# if the contents of the database file (SRC_DATABASE) changed, e.g.,
# if "add" was a target on the same make command line, we may be
# missing some files; the other files will have been updated (if
# necessary) by the ${BUILT_TAGGER_THUMBS} prerequisite
define _deficit
.PHONY: $1
$1:
	if [ -f ${3} ]; then \
		indb=`${JQ} '.[]|.path' ${3} | sed 's/"//g'`; \
		for pic in $$$${indb}; do \
			if [ ! -f ${2}/$$$${pic} ]; then \
				convert "${SRC_IMAGES_DIR}/$$$${pic}" \
					-resize ${THUMBNAIL_GEOM} \
					"${2}/$$$${pic}"; \
			fi \
		done \
	fi
endef
# if files, previously included, have been *excluded*, from the
# database, this will get rid of them.  we use an array to hold what
# should be there, and then check each file that *is* there, removing
# those not found in the array
define _excess
.PHONY: $1
$1:
	if [ -f ${3} ]; then \
		declare -A pics; \
		indb=`${JQ} '.[]|.path' ${3} | sed 's/"//g'`; \
		existing=`ls ${2}`; \
		for pic in $$$${indb}; do \
			pics[$$$${pic}]="exists"; \
		done; \
		for pic in $$$${existing}; do \
			if [ ! "$$$${pics[$$$$pic]}" ]; then \
				echo removing excess file ${2}/$$$${pic}; \
				rm -f ${2}/$$$${pic}; \
			fi \
		done \
	fi

endef
# deficits (tagger *images* come straight from the source, SRC_IMAGES_DIR)
$(eval $(call _deficit,tagger_thumbs_deficit,${BUILT_TAGGER_THUMBS_DIR},${SRC_DATABASE}))
$(eval $(call _deficit,viewer_site_images_deficit,${BUILT_VIEWER_SITE_IMAGES_DIR},${BUILT_VIEWER_DATABASE}))
$(eval $(call _deficit,viewer_site_thumbs_deficit,${BUILT_VIEWER_SITE_THUMBS_DIR},${BUILT_VIEWER_DATABASE}))
# excesses (ditto)
$(eval $(call _excess,tagger_thumbs_excess,${BUILT_TAGGER_THUMBS_DIR},${SRC_DATABASE}))
$(eval $(call _excess,viewer_site_images_excess,${BUILT_VIEWER_SITE_IMAGES_DIR},${BUILT_VIEWER_DATABASE}))
$(eval $(call _excess,viewer_site_thumbs_excess,${BUILT_VIEWER_SITE_THUMBS_DIR},${BUILT_VIEWER_DATABASE}))


# TAGGER IMAGES and THUMBS

.PHONY: tagger_images
tagger_images: ${SRC_TAGGER_IMAGES}

.PHONY: tagger_thumbs
${BUILT_TAGGER_THUMBS_DIR}:
	mkdir -p $@
tagger_thumbs: ${BUILT_TAGGER_THUMBS_DIR} ${BUILT_TAGGER_THUMBS} \
							tagger_thumbs_excess tagger_thumbs_deficit


# VIEWER IMAGES and THUMBS

# VIEWER *SITE* IMAGES and THUMBS

.PHONY: viewer_site_images
${BUILT_VIEWER_SITE_IMAGES_DIR}:
	mkdir -p $@
viewer_site_images: ${BUILT_VIEWER_SITE_IMAGES_DIR} \
							viewer_site_images_excess viewer_site_images_deficit

.PHONY: viewer_site_thumbs
${BUILT_VIEWER_SITE_THUMBS_DIR}:
	mkdir -p $@
viewer_site_thumbs: ${BUILT_VIEWER_SITE_THUMBS_DIR} \
							viewer_site_thumbs_excess viewer_site_thumbs_deficit

# here we pick up things from the photoTagger r-package
PT_DIR = $(shell Rscript -e "cat(system.file('', package='photoTagger'))")
PT_BIN = ${PT_DIR}/bin
PT_BUILD_JSON = ${PT_BIN}/build-json.sh
PT_INIT = ${PT_BIN}/pt-init
PT_ADD = ${PT_BIN}/pt-add
PT_TAG = ${PT_BIN}/pt-tag
PT_VIEW = ${PT_BIN}/pt-view
PT_VIEWER_BUILD = ${PT_BIN}/pt-viewer-build

# INIT

# given a set of images, create a database, add the images

# actually do the work.  "called" by the init: target
_init:
	${PT_INIT} --database ${SRC_DATABASE}

init: _init

# IMAGESADDED

# given a directory of images (and, *only* images), merge into, the
# database
add:
	${PT_ADD} \
		--database ${SRC_DATABASE} \
		--images-dir ${SRC_IMAGES_DIR}

# TAGGER

tag: ${SRC_TAGS} tagger_images tagger_thumbs
	-${PT_TAG} ${VERBOSE} ${BROWSER} ${PERSIST} \
			--images-dir ${SRC_IMAGES_DIR} \
			--thumbnails-dir ${BUILT_TAGGER_THUMBS_DIR} \
			--database ${SRC_DATABASE} \
			--tags ${SRC_TAGS} ${OPT_PREFILTER} \
			--gallery-kind ${GALLERY_KIND} \
			--filters-style ${FILTERS_STYLE} \
			--title "${TITLE}"
# view

view: ${SRC_TAGS} tagger_images tagger_thumbs
	-${PT_VIEW} ${VERBOSE} ${BROWSER} ${PERSIST} \
			--images-dir ${SRC_IMAGES_DIR} \
			--thumbnails-dir ${BUILT_TAGGER_THUMBS_DIR} \
			--database ${SRC_DATABASE} \
			--tags ${SRC_TAGS} ${OPT_PREFILTER} ${OPT_RANDOMIZE} ${OPT_RANDOMIZEFRAC} \
			--gallery-kind ${GALLERY_KIND} \
			--filters-style ${FILTERS_STYLE} \
			--title "${TITLE}"
# VIEWER BUILD

${BUILT_VIEWER_SITE_DIR}:
	mkdir -p $@

viewerbuild: ${BUILT_VIEWER_SITE_DIR} \
			${BUILT_VIEWER_SITE_IMAGES_DIR} \
			${SRC_TAGS} \
			viewer_database viewer_site_images viewer_site_thumbs
	${PT_VIEWER_BUILD} ${VERBOSE} \
			--images-dir ${SRC_IMAGES_DIR} \
			--thumbnails-dir ${BUILT_VIEWER_SITE_THUMBS_DIR} \
			--database ${BUILT_VIEWER_DATABASE} \
			--tags ${SRC_TAGS} ${OPT_PREFILTER} ${OPT_RANDOMIZE} ${OPT_RANDOMIZEFRAC} \
			--gallery-kind ${GALLERY_KIND} \
			--filters-style ${FILTERS_STYLE} \
			--title "${TITLE}" \
			--build-dir ${BUILT_VIEWER_SITE_DIR} \
			--build-update
# CLEANers

# clean up after ourselves
viewerclean:
	-rm -rf ${BUILT_VIEWER_SITE_DIR}
	-rm -rf ${BUILT_COMMON_DIR}
	-rm -rf ${BUILT_FOR_VIEWER_DIR}
	-rm -rf ${BUILT_FOR_TAGGER_DIR}
